Tests for file/listfolder

Scenario: Check list of files for empty user
Meta:
@userType empty
Given Set authorization for user
When Send request to get list of folders
Then Verify successful status code
Then Verify response body
And Response matches default empty body
When Set default folderId from respose
Then Send request with folderId
Then Verify response with folderId
And Response matches empty file list body


Scenario: Check list of files for user
Meta:
@userType valid
Given Set authorization for user
When Send request to get list of folders
Then Verify successful status code
Then Verify response body
And Folder list is not empty
And File list is not empty
When Set folderId from respose
Then Send request with folderId
Then Verify response with folderId
And File list is not empty