Tests for /account/info

Scenario: Check info for valid account
Meta:
@userType valid
Given Set authorization for user
When Send request to get account details
Then Verify successful status code
Then Verify received response


Scenario: Check info for empty account
Meta:
@userType empty
Given Set authorization for user
When Send request to get account details
Then Verify successful status code
Then Verify received response


Scenario: Check info for invalid account
Meta:
@userType invalid
Given Set authorization for user
When Send request to get account details
Then Confirm received response status: 403
Then Confirm received message: 'Authentication failed'