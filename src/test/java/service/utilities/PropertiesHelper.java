package service.utilities;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import org.assertj.core.util.Strings;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import service.BaseClass;

import java.io.IOException;
import java.util.*;
import java.util.regex.Pattern;

import static org.junit.Assert.fail;

public class PropertiesHelper extends BaseClass {
    private final static Logger LOG = LoggerFactory.getLogger(PropertiesHelper.class.getName());


    protected static JsonNode rootNode(final String jsonFile) {
        String filePath = Strings.concat("src\\test\\resources\\", jsonFile, ".json");

        ObjectMapper objectMapper = new ObjectMapper();
        JsonNode rootNode = null;
        try {
            rootNode = objectMapper.readTree(generateStringFromFile(filePath));
        } catch (IOException e) {
            fail(e.getMessage());
        }
        return rootNode;
    }

    protected static Map<String, String> getMapOfStrings(final JsonNode node) {
        Map<String, String> stringMap = new HashMap<>();
        getStringObjectMap(node).keySet().forEach(s -> stringMap.put(s, getStringObjectMap(node).get(s).toString()));
        return stringMap;
    }

    private static Map<String, Object> getStringObjectMap(final JsonNode node) {
        Map<String, Object> stringObjectMap = new HashMap<>();

        Iterator<String> fieldNames = node.fieldNames();
        while (fieldNames.hasNext()) {
            String fieldName = fieldNames.next();
            JsonNode fieldValue = node.get(fieldName);
            if (fieldValue.isObject()) {
                stringObjectMap.put(fieldName, fieldValue);
            } else {
                stringObjectMap.put(fieldName, fieldValue.asText());
            }
        }
        return stringObjectMap;
    }

    protected List<String> objectToStringArray(final JsonNode rootNode, String jsonPath) {
        String[] jsonKeys = jsonPath.split(Pattern.quote("."));

        List<String> result = new ArrayList<>();
        ArrayNode slaidsNode = (ArrayNode) rootNode.get(jsonKeys[0]).get(jsonKeys[1]);
        Iterator<JsonNode> slaidsIterator = slaidsNode.elements();
        while (slaidsIterator.hasNext()) {
            JsonNode slaidNode = slaidsIterator.next();
            result.add(slaidNode.asText());
        }
        return result;
    }
}
