package service.utilities;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.collect.Iterators;
import org.assertj.core.util.Strings;
import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;
import java.util.regex.Pattern;

import static org.junit.Assert.fail;

public class Utilities extends PropertiesHelper {
    private final static Logger LOG = LoggerFactory.getLogger(Utilities.class.getName());

    public static int getRandom(int maxNum) {
        return ThreadLocalRandom.current().nextInt(0, maxNum);
    }

    public static <T> T randomObjectFromList(List<T> list) {
        if(list != null && !(list.isEmpty()))
            return list.get(getRandom(list.size() - 1));
        else
            return null;
    }

    public static boolean stringComparator(final String given, final String expected){
        return !Strings.isNullOrEmpty(given) && given.equals(expected);
    }

    public String replaceArg(final String sentence, final String arg){
        return replaceArgs(sentence, Collections.singletonList(arg));
    }

    public static String[] splitByDot(final String parameter) {
        return parameter.split(Pattern.quote("."));
    }

    public static boolean isEmptyOrNull(Collection<Object> collection) {
        return (collection == null || collection.isEmpty());
    }

    public static <T> T getLastElement(Iterable<T> iterable) {
        if (iterable instanceof List) {
            List<T> list = (List<T>) iterable;
            if (list.isEmpty()) {
                return null;
            }
            return list.get(list.size() - 1);
        }
        return Iterators.getLast(iterable.iterator());
    }

    public String replaceArgs(final String sentence, final List<String> argList) {
        String quote = sentence;

        for (String arg : argList) {
            int start = quote.indexOf("{");
            int end = quote.indexOf("}") + 1;

            quote = quote.replace(quote.substring(start, end), arg);

            LOG.info(quote);
        }
        return quote;
    }

    public JsonNode objectToJsonNode(Object object) {
        ObjectMapper objectMapper = new ObjectMapper();
        JsonNode root = null;
        try {
            root = objectMapper.readTree(String.valueOf(object));
        } catch (IOException e) {
            fail(e.getMessage());
        }
        return root;
    }

    public static JSONObject simpleJsonReader(final String resourcePath){
        String JSON_TEMPLATE = generateStringFromFile("src\\test\\resources\\" + resourcePath);

        if(Strings.isNullOrEmpty(JSON_TEMPLATE))
            fail("RESOURCE LOADING FAILURE!");
        return new JSONObject(JSON_TEMPLATE);
    }

    public static List<JSONObject> simpleJSONArray(final String relativePath, final String jsonPath){
        List<JSONObject> arrayBody = new ArrayList();
        JSONObject object = simpleJsonReader(relativePath);

        String[] json = jsonPath.split(Pattern.quote("."));

        JSONArray jsonArray = object.getJSONObject(json[0]).getJSONArray(json[1]);

        for (int i = 0; i < jsonArray.length(); ++i) {
            arrayBody.add(jsonArray.getJSONObject(i));
        }
        return arrayBody;
    }
}
