package service.utilities;

import io.restassured.http.ContentType;
import io.restassured.response.Response;
import net.thucydides.core.annotations.Step;
import service.BaseClass;
import service.openload.RequestData;

import java.util.Map;

import static net.serenitybdd.rest.SerenityRest.given;
import static service.openload.OpenloadHelper.apiKeysForUser;

public class RequestHelper extends BaseClass {
    private static RequestData REQUEST_DATA = new RequestData();

    private static final String EMAIL = "email";

    public void getRequest(final String request) {
        Response response = given()
                                .contentType(ContentType.JSON)
                                .when()
                                .get(request)
                                .prettyPeek()
                                .then()
                                .extract()
                                .response();
        setResponse(response);
    }

    public void postRequest(final String request) {
        Response response = given()
                                .contentType(ContentType.JSON)
                                .when()
                                .post(request)
                                .prettyPeek()
                                .then()
                                .extract()
                                .response();

        setResponse(response);
    }

    protected RequestData authorizationData(){
        return REQUEST_DATA;
    }

    @Step
    public void userAuthorization(final String userType){
        Map<String, String> userData = apiKeysForUser(userType);

        REQUEST_DATA.setLogin(userData.get("api_login"));
        REQUEST_DATA.setKey(userData.get("api_key"));

        saveValue(EMAIL, apiKeysForUser(userType).get(EMAIL));
    }
}
