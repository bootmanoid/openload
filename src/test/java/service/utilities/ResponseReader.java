package service.utilities;

import io.restassured.path.json.JsonPath;
import org.json.JSONArray;
import org.json.JSONObject;
import service.BaseClass;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class ResponseReader extends BaseClass {

    public JSONObject getResponseAsJson(){
        String RESPONSE_JSON =  getResponse().getBody().asString();
        return new JSONObject(RESPONSE_JSON);
    }

    public List<JSONObject> jsonArrayFromResponse(final String jsonPath){
        List<JSONObject> arrayBody = new ArrayList();

        JSONArray jsonArray = getResponseAsJson().getJSONArray(jsonPath);

        for (int i = 0; i < jsonArray.length(); ++i) {
            arrayBody.add(jsonArray.getJSONObject(i));
        }
        return arrayBody;
    }

    public String getFromResponse(final String parameter) {
        JsonPath root = getResponse().getBody().jsonPath().setRoot("result");
        return root.getString(parameter);
    }

    public int getResponseStatus() {
        return Integer.parseInt(getResponse().getBody().jsonPath().getString("status"));
    }

    public String getResponseMessage() {
        return getResponse().getBody().jsonPath().getString("msg");
    }

    public int getStatusCode() {
        return getResponse().getStatusCode();
    }

    public JsonPath responseBody() {
        return getResponse().getBody().jsonPath().setRoot("result");
    }

    public List<Map> getFolderList() {
        return responseBody().getList("folders", Map.class);
    }

    public List<Map> getFileList() {
        return responseBody().getList("files", Map.class);
    }
}
