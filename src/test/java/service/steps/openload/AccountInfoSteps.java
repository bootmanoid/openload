package service.steps.openload;

import org.apache.http.HttpStatus;
import org.jbehave.core.annotations.Given;
import org.jbehave.core.annotations.Named;
import org.jbehave.core.annotations.Then;
import org.jbehave.core.annotations.When;
import service.openload.accountInfo.AccountInfo;
import service.utilities.ResponseReader;

import static org.junit.Assert.assertEquals;

public class AccountInfoSteps {
    private static ResponseReader RESPONSE_READER = new ResponseReader();
    private static AccountInfo STEPS = new AccountInfo();


    @Given("Set authorization for user")
    public void setAuthData(@Named("userType") String userType){
        STEPS.setAccountDetails(userType);
    }

    @When("Send request to get account details")
    public void getAccountDetails(){
        STEPS.sendRequest();
    }

    @Then("Verify successful status code")
    public void verifyStatus(){
        assertEquals(HttpStatus.SC_OK, RESPONSE_READER.getStatusCode());
    }

    @Then("Verify received response")
    public void verifyResponse(){
        STEPS.verifyResponseBody();
    }

    @Then("Confirm received response status: 403")
    public void statusCodeInBody(){
        assertEquals(HttpStatus.SC_FORBIDDEN, RESPONSE_READER.getResponseStatus());
    }

    @Then("Confirm received message: '$message'")
    public void verifyReceivedMssg(@Named("message") String message){
        assertEquals(message, RESPONSE_READER.getResponseMessage());
    }
}
