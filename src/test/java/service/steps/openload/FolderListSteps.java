package service.steps.openload;

import org.jbehave.core.annotations.Named;
import org.jbehave.core.annotations.Then;
import org.jbehave.core.annotations.When;
import service.openload.folderList.FolderList;

import static service.utilities.Utilities.stringComparator;

public class FolderListSteps {
    private static FolderList STEPS = new FolderList();

    @When("Send request to get list of folders")
    public void requestFolders(@Named("userType") final String userType){
        STEPS.sendBasicRequest();
    }

    @Then("Verify response body")
    public void verifier(){
        STEPS.verifyResponseBody();
    }

    @When("Set $folderType folderId from respose")
    public void setFolderIdType(@Named("folderType") String folderType){
        if(stringComparator(folderType, "default"))
            STEPS.setFolderIdAsDefault();
        else
            STEPS.setExistingFolderId();
    }

    @When("Set folderId from respose")
    public void setFolderId(){
        STEPS.setExistingFolderId();
    }

    @Then("Send request with folderId")
    public void sendWithId(){
        STEPS.sendRequestWithId();
    }

    @Then("Verify response with folderId")
    public void verifyResFolderId(){
        STEPS.folerExistsResponse();
    }

    @Then("File list is not empty")
    public void fileListNotEmpty(){ STEPS.fileListNotEmpty(); }

    @Then("Folder list is not empty")
    public void folderListNotEmpty(){ STEPS.folderListNotEmpty(); }

    @Then("Response matches default empty body")
    public void defaultEmptyBody(){
        STEPS.emptyBodyByDefault();
    }

    @Then("Response matches empty file list body")
    public void defaultEmptyFileList(){
        STEPS.emptyFileList();
    }
}
