package service.openload;

import com.google.common.base.Strings;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class RequestData {
    private static String login;
    private static String key;

    public String getLogin() {
        return login;
    }

    public String getKey() {
        return key;
    }

    public void setLogin(final String login) {
        RequestData.login = login;
    }

    public void setKey(final String key) {
        RequestData.key = key;
    }

    protected static List<String> listOfParams() {
        return Stream
                .of(login, key)
                .filter(value -> !Strings.isNullOrEmpty(value))
                .collect(Collectors.toList());
    }
}