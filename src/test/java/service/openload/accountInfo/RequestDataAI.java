package service.openload.accountInfo;

import service.openload.OpenloadHelper;
import service.openload.RequestData;
import service.utilities.Utilities;

class RequestDataAI extends RequestData {
    private static final Utilities tool = new Utilities();

    private static final String ACCOUNT_INFO = "accountInfo";

    static String getRequestUrl(){
        return tool.replaceArgs(OpenloadHelper.getUrl(ACCOUNT_INFO), listOfParams());
    }
}
