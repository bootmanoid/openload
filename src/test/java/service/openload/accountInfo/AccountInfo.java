package service.openload.accountInfo;

import net.thucydides.core.annotations.Step;
import org.apache.http.HttpStatus;
import service.utilities.RequestHelper;
import service.utilities.ResponseReader;

import java.util.Map;

import static org.junit.Assert.assertEquals;
import static service.openload.OpenloadHelper.apiKeysForUser;

public class AccountInfo extends RequestHelper {
    private static ResponseReader RESPONSE_READER = new ResponseReader();

    private static final String EMAIL = "email";

    @Step
    public void setAccountDetails(final String userType){
        Map<String, String> userData = apiKeysForUser(userType);

        userAuthorization(userType);
        saveValue(EMAIL, userData.get(EMAIL));
    }

    @Step
    public void sendRequest(){
        getRequest(RequestDataAI.getRequestUrl());
    }

    @Step
    public void verifyResponseBody() {
        assertEquals(
                HttpStatus.SC_OK,
                RESPONSE_READER.getResponseStatus());
        assertEquals(
                authorizationData().getLogin(),
                RESPONSE_READER.getFromResponse("extid"));
        assertEquals(
                getSavedValue(EMAIL),
                RESPONSE_READER.getFromResponse(EMAIL));
    }
}
