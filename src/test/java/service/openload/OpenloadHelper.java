package service.openload;

import org.assertj.core.util.Strings;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import service.utilities.PropertiesHelper;

import java.util.HashMap;
import java.util.Map;

import static org.junit.Assert.fail;

public class OpenloadHelper extends PropertiesHelper {
    private final static Logger LOG = LoggerFactory.getLogger(PropertiesHelper.class.getName());
    private final static String OPENLOAD = "properties_openload";

    private final static String VALID = "valid";
    private final static String EMPTY = "empty";
    private final static String INVALID = "invalid";


    public static String getUrl(final String method) {
        return Strings.concat(
                getMapOfStrings(rootNode(OPENLOAD).get("base")).get("url"),
                getMapOfStrings(rootNode(OPENLOAD).get(method)).get("url"));
    }

    public static Map<String, String> getDataForMethod(final String method){
        return getMapOfStrings(rootNode(OPENLOAD).get(method));
    }

    private static Map<String, String> getUserData(final String userType){
        switch (userType) {
            case VALID:
                return getMapOfStrings(rootNode(OPENLOAD).get("userData"));
            case INVALID:
                return getMapOfStrings(rootNode(OPENLOAD).get("invalidAccount"));
            case EMPTY:
                return getMapOfStrings(rootNode(OPENLOAD).get("emptyAccount"));
            default:
                fail();
                return new HashMap<>();
        }
    }

    public static Map<String, String> apiKeysForUser(final String userType) {
        Map<String, String> mapOfStrings = getUserData(userType);
        mapOfStrings.keySet().removeIf(o -> !o.contains("api") && !o.contains("email"));
        return mapOfStrings;
    }
}
