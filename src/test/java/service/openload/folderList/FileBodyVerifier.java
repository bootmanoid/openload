package service.openload.folderList;

import com.google.common.collect.ImmutableMap;

import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;

import static java.util.regex.Pattern.compile;
import static org.junit.Assert.fail;

class FileBodyVerifier {
    private final Map<String, Pattern> FILE_BODY = new ImmutableMap.Builder<String, Pattern>() {
    }
            .put("name", compile("\\w+.+\\w"))
            .put("cblock", compile(".*"))
            .put("sha1", compile("^[a-zA-Z0-9]+"))
            .put("folderid", compile("^\\d{1,9}"))
            .put("upload_at", compile("^\\d{10}"))
            .put("status", compile("^active"))
            .put("size", compile("^\\d+"))
            .put("content_type", compile("\\w+/+.+"))
            .put("download_count", compile("^\\d+"))
            .put("cstatus", compile("^ok"))
            .put("link", compile("https://openload.co/f/[a-zA-Z0-9-]{2,}/\\w+.+\\w"))
            .put("linkextid", compile("[a-zA-Z0-9-]{2,}"))
            .build();


    private boolean matches(String name, Object value) {
        return String.valueOf(value).matches(FILE_BODY.get(name).pattern());
    }

    @SuppressWarnings("unchecked")
    void verifyFileList(List<Map> fileList) {
        for (Map map : fileList) {
            if(map.isEmpty()){
                break;
            } else if(map.keySet().stream().anyMatch(key -> !matches(key.toString(), map.get(key)))){
                fail();
            }
        }
    }

}
