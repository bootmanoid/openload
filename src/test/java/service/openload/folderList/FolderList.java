package service.openload.folderList;

import net.thucydides.core.annotations.Step;
import org.apache.http.HttpStatus;
import org.json.JSONObject;
import service.openload.model.FileBody;
import service.utilities.RequestHelper;
import service.utilities.ResponseReader;
import service.utilities.Utilities;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static org.junit.Assert.*;

public class FolderList extends RequestHelper {
    private static ResponseReader RESPONSE_READER = new ResponseReader();
    private static FileBodyVerifier FILE_LIST_VERIFIER = new FileBodyVerifier();

    private static final String INVALID_FOLDER_MSSG = "this is not your folder";
    private static final String EMPTY_USER_BODY_PATH = "fileList\\defaultBodyForEmpty.json";
    private static final String EMPTY_FILE_LIST_PATH = "fileList\\emptyFileList.json";

    @Step
    public void setFolderId(final String folderID){
        RequestDataFL.setFolderId(folderID);
    }

    @Step
    public void sendBasicRequest(){
        getRequest(RequestDataFL.getRequestUrl());
    }

    @Step
    public void sendRequestWithId(){
        getRequest(RequestDataFL.requestUrlWithFolder());
    }

    @Step
    public void verifyResponseBody() {
        assertEquals(
                HttpStatus.SC_OK,
                RESPONSE_READER.getResponseStatus());
        assertNotNull(
                RESPONSE_READER.getFolderList());

        List<Map> fileList = RESPONSE_READER.getFileList();

        if(!fileList.isEmpty()) {
            FILE_LIST_VERIFIER.verifyFileList(fileList);
        }
    }

    @Step
    public void setFolderIdFromFiles(){
        Map map = Utilities.randomObjectFromList(RESPONSE_READER.getFileList());
        FileBody fileBody = new FileBody(map);
        RequestDataFL.setFolderId(fileBody.getFolderId());
    }


     private List<String> defaultFolders(){
         return Utilities
                .simpleJSONArray(EMPTY_USER_BODY_PATH, "result.folders")
                .stream()
                .map(n -> n.getString("name"))
                .collect(Collectors.toList());
    }

    @Step
    public void setExistingFolderId(){
        List<Map> folderList = RESPONSE_READER.getFolderList();
        List<String> ids = folderList
                .stream()
                .filter(map -> !defaultFolders().contains(map.get("name").toString()))
                .map(map -> map.get("id").toString())
                .collect(Collectors.toList());

        if(!ids.isEmpty()){
            RequestDataFL.setFolderId(Utilities.randomObjectFromList(ids));
        } else
            fail("User doesn't have any non-default folder!");
    }

    public void setFolderIdAsDefault(){
        List<Map> folderList = RESPONSE_READER.getFolderList();
        List<String> ids = folderList
                .stream()
                .filter(map -> defaultFolders().contains(map.get("name").toString()))
                .map(map -> map.get("id").toString())
                .collect(Collectors.toList());

        RequestDataFL.setFolderId(Utilities.randomObjectFromList(ids));
    }

    @Step
    public void folerExistsResponse(){
        assertEquals(
                HttpStatus.SC_OK,
                RESPONSE_READER.getResponseStatus());
        assertTrue(
                RESPONSE_READER.getFolderList().isEmpty());

        List<Map> fileList = RESPONSE_READER.getFileList();

        if(!fileList.isEmpty()) {
            FILE_LIST_VERIFIER.verifyFileList(fileList);
        }
    }

    @Step
    public void setInvalidFolderId(){
        RequestDataFL.setFolderId("0000000");
    }

    @Step
    public void invalidFolderResponse(){
        assertEquals(
                HttpStatus.SC_FORBIDDEN,
                RESPONSE_READER.getResponseStatus());

        assertEquals(
                INVALID_FOLDER_MSSG,
                RESPONSE_READER.getResponseMessage()
        );
        assertNull(RESPONSE_READER.responseBody().get());
    }

    @Step
    public void fileListNotEmpty(){
        assertFalse(RESPONSE_READER.getFileList().isEmpty());
    }

    @Step
    public void folderListNotEmpty(){
        List<String> defaults = defaultFolders();

        List<Map> folderList = RESPONSE_READER.getFolderList();
        List<Map> name = folderList
                .stream()
                .filter(n -> !defaults.contains(n.get("name").toString()))
                .collect(Collectors.toList());

        assertFalse(name.isEmpty());
    }

    @Step
    public void emptyBodyByDefault(){
        List<JSONObject> jsonObjects = Utilities.simpleJSONArray(EMPTY_USER_BODY_PATH, "result.folders");

        assertEquals(
                jsonObjects.size(), RESPONSE_READER.getFolderList().size());
        assertTrue(
                RESPONSE_READER.getFileList().isEmpty());
    }

    @Step
    public void emptyFileList(){
        assertEquals(
                RESPONSE_READER.getResponseAsJson().toString().trim(),
                Utilities.simpleJsonReader(EMPTY_FILE_LIST_PATH).toString().trim()
        );
    }
}