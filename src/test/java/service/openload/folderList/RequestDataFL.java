package service.openload.folderList;

import org.assertj.core.util.Strings;
import service.openload.OpenloadHelper;
import service.openload.RequestData;
import service.utilities.Utilities;

import static java.util.Collections.*;

public class RequestDataFL extends RequestData {
    private static final Utilities tool = new Utilities();

    private static final String LIST_FOLDER = "listFolder";
    private static final String FOLDERS_SUFFIX = "folderSuffix";

    private static String folderId;

    public static void setFolderId(String folderID) {
        RequestDataFL.folderId = folderID;
    }

    static String getRequestUrl(){
        return tool.replaceArgs(OpenloadHelper.getUrl(LIST_FOLDER), listOfParams());
    }


    private static String getURLSuffix(){
        String suffixURL = OpenloadHelper.getDataForMethod(LIST_FOLDER).get(FOLDERS_SUFFIX);
        return tool.replaceArgs(suffixURL, singletonList(folderId));
    }

    static String requestUrlWithFolder(){
        return Strings.concat(getRequestUrl(), getURLSuffix());
    }
}
