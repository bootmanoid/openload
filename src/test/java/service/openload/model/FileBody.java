package service.openload.model;

import java.util.Map;

public class FileBody {
    private String name;
    private Object cblock;
    private String sha1;
    private String folderid;
    private String upload_at;
    private String status;
    private String size;
    private String content_type;
    private String download_count;
    private String cstatus;
    private String link;
    private String linkextid;


    public FileBody(Map map) {
        super();
        this.name = map.get("name").toString();
        this.cblock = map.get("cblock");
        this.sha1 = map.get("sha1").toString();
        this.folderid = map.get("folderid").toString();
        this.upload_at = map.get("upload_at").toString();
        this.status = map.get("status").toString();
        this.size = map.get("size").toString();
        this.content_type = map.get("content_type").toString();
        this.download_count = map.get("download_count").toString();
        this.cstatus = map.get("cstatus").toString();
        this.link = map.get("link").toString();
        this.linkextid = map.get("linkextid").toString();
    }

    public String getName() {
        return name;
    }

    public Object getCblock() {
        return cblock;
    }

    public String getSha1() {
        return sha1;
    }

    public String getFolderId() {
        return folderid;
    }

    public String getUploadAt() {
        return upload_at;
    }

    public String getStatus() {
        return status;
    }

    public String getSize() {
        return size;
    }

    public String getContentType() {
        return content_type;
    }

    public String getDownloadCount() {
        return download_count;
    }

    public String getCstatus() {
        return cstatus;
    }

    public String getLink() {
        return link;
    }

    public String getLinkextId() {
        return linkextid;
    }
}
