package service.guerrillamail;

import org.assertj.core.util.Strings;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import service.utilities.PropertiesHelper;

public class GuerrillaHelper extends PropertiesHelper {
    private final static Logger LOG = LoggerFactory.getLogger(PropertiesHelper.class.getName());
    private final static String GUERRILLA = "properties_guerrilla";

    public static String getRequestUrl(String method) {
        return Strings.concat(
                PropertiesHelper.getMapOfStrings(PropertiesHelper.rootNode(GUERRILLA).get("base")).get("url"),
                PropertiesHelper.getMapOfStrings(PropertiesHelper.rootNode(GUERRILLA).get(method)).get("url"));
    }


}
