package service;

import io.restassured.response.Response;
import net.serenitybdd.core.Serenity;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

public class BaseClass {
    private final static Logger LOG = LoggerFactory.getLogger(BaseClass.class.getName());
    private static final String RESPONSE = "response";

    protected void clearSession() {
        Serenity.clearCurrentSession();
    }

    protected void setResponse(Response response) {
        Serenity.setSessionVariable(RESPONSE).to(response);
    }

    protected Response getResponse() {
        return (Response) Serenity.getCurrentSession().get(RESPONSE);
    }

    protected void saveValue(final String id, final String variable) {
        Serenity.setSessionVariable(id).to(variable);
    }

    protected String getSavedValue(final String id) {
        return (String) Serenity.getCurrentSession().get(id);
    }

    protected static String generateStringFromFile(String path){
        try {
            return new String(Files.readAllBytes(Paths.get(path)));
        } catch (IOException e) {
            LOG.error(e.getMessage());
        }
        return "";
    }
}